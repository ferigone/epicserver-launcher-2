﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Launcher
{
    public partial class VersionInstallWindow : Form
    {
        public MainWindow mainWindow;
        private VersionsIdentifier identifier;
        private VersionsIdentifier.Versions currentSelectedVersion;

        private static string releaseTimeLabelPrefix = "Release time: ";
        private static string versionsUrl = "http://s3.amazonaws.com/Minecraft.Download/versions/";
        private static string librariesUrl = "https://libraries.minecraft.net/";

        public VersionInstallWindow(MainWindow mainWindow)
        {
            InitializeComponent();

            

            this.mainWindow = mainWindow;

            typeComboBox.SelectedItem = typeComboBox.Items[0];
            versionComboBox.SelectedItem = versionComboBox.Items[0];
            releaseTimeLabel.Text = releaseTimeLabelPrefix;

            RefreshVersionsList();

            Show();
            
            this.BringToFront();
        }

        private void RefreshVersionsList()
        {
            identifier = VersionsIdentifier.Identify(FetchUrl(versionsUrl + "versions.json"));

            if (identifier == null)
            {
                mainWindow.AddConsoleLine("Unable to get versions from s3.amazonaws.com");
                return;
            }

            versionComboBox.Items.Clear();
            foreach (VersionsIdentifier.Versions version in identifier.versions)
            {
                if (version.type == null)
                    continue;
                if (version.type == typeComboBox.SelectedItem.ToString().ToLower())
                    versionComboBox.Items.Add(version.id);
            }

            versionComboBox.SelectedItem = versionComboBox.Items[0];
        }

        /// <summary>
        /// Get versions.json and identifies it, then returns it if successful.
        /// Returns NULL if an error occurred.
        /// </summary>
        /// <returns></returns>
        public string FetchUrl(string url)
        {
            // used to build entire input
            StringBuilder sb = new StringBuilder();

            // used on each read operation
            byte[] buf = new byte[8192];

            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(url);

            // execute the request
            HttpWebResponse response = (HttpWebResponse)
                request.GetResponse();
            // we will read data via the response stream
            Stream resStream = response.GetResponseStream();
            string tempString = null;
            int count = 0;

            do
            {
                // fill the buffer with data
                count = resStream.Read(buf, 0, buf.Length);

                // make sure we read some data
                if (count != 0)
                {
                    // translate from bytes to ASCII text
                    tempString = Encoding.ASCII.GetString(buf, 0, count);

                    // continue building the string
                    sb.Append(tempString);
                }
            }
            while (count > 0); // any more data to read?

            return sb.ToString();
        }

        /// <summary>
        /// Close the window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitBtn_Click(object sender, EventArgs e)
        {
            // During the installation process, the main window is not displayed in the taskbar.
            // We check if this installation process is ongoing, because we want to close the application
            // entirely. If it is not ongoing, the user probably started this window even though MC is installed.
            if (mainWindow.settings == null || !mainWindow.ShowInTaskbar)
                Application.Exit();
            else
                Close();
        }

        /// <summary>
        /// Executed when the user changed the selected type.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshVersionsList();
        }

        /// <summary>
        /// Executed when the selected version changed.
        /// It updates the release time display and changes
        /// current version variable.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void versionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (versionComboBox.SelectedItem == null || identifier == null)
                return;

            foreach(VersionsIdentifier.Versions version in identifier.versions)
            {
                if(versionComboBox.SelectedItem.ToString() == version.id)
                {
                    currentSelectedVersion = version;
                    releaseTimeLabel.Text = releaseTimeLabelPrefix + currentSelectedVersion.releaseTime;
                    break;
                }
            }
        }

        /// <summary>
        /// Install the currently selected version of minecraft.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void installBtn_Click(object sender, EventArgs e)
        {
            // Overwrite the default version with the chosen version
            mainWindow.settings.launcherSettings.minecraftVersion = currentSelectedVersion.id + ".jar";


            string jsonUrl = versionsUrl + currentSelectedVersion.id + "/" + currentSelectedVersion.id + ".json";
            string urlResponse = FetchUrl(jsonUrl);

            MCIdentifier identifier = MCIdentifier.parse(urlResponse);

            if (identifier == null)
            {
                MessageBox.Show(identifier.id);
                mainWindow.AddConsoleLine("(Install) Unable to get or identify the version's json data.");
                return;
            }
            else
            {
                if (!Directory.Exists(MainWindow.minecraftVersionsDir))
                    Directory.CreateDirectory(MainWindow.minecraftVersionsDir);
                if (!Directory.Exists(String.Format("{0}/{1}", MainWindow.minecraftVersionsDir, currentSelectedVersion.id)))
                    Directory.CreateDirectory(String.Format("{0}/{1}", MainWindow.minecraftVersionsDir, currentSelectedVersion.id));

                string targetPath = String.Format("{0}/{1}/{1}.json", MainWindow.minecraftVersionsDir, currentSelectedVersion.id);
                FileStream stream = File.Create(targetPath);
                stream.Close();
                StreamWriter writer = new StreamWriter(targetPath);
                writer.Write(urlResponse);
                writer.Close();
            }

            mainWindow.Show();
            new DownloadProgressWindow(mainWindow, identifier);
            Close();

        }


    }
}
