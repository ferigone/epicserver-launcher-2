﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Windows.Forms;
using System.IO;

namespace Launcher
{
    public class LauncherUpdater
    {
        private MainWindow mainWindow = null;
        private static string mainVersionURL = "http://www.epicserver.nl/GCL/GCLVersion.json";

        public LauncherUpdater(MainWindow creator)
        {
            mainWindow = creator;
            mainWindow.AddConsoleLine("(Updater) Checking for updates.");
            CheckForUpdates();
        }

        /// <summary>
        /// Check if the update server is online.
        /// returns TRUE if it is online.
        /// </summary>
        public bool IsServerOnline
        {
            get
            {
                HttpWebResponse response = null;
                var request = (HttpWebRequest)WebRequest.Create("http://www.epicserver.nl");
                request.Method = "HEAD";
                request.Timeout = 1000;

                bool isOnline = false;

                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                catch (WebException ex)
                {
                    /* A WebException will be thrown if the status of the response is not `200 OK` */
                    isOnline = false;
                }
                finally
                {
                    // Don't forget to close your response.
                    if (response != null)
                    {
                        response.Close();
                        isOnline = true;
                    }
                }

                return isOnline;

                // OLD CODE. TODO remove this later if it works after a couple of days.
                /*try
                {
                    using (var client = new WebClient())
                    using (var stream = client.OpenRead("http://www.epicserver.nl"))
                    {
                        return true;
                    }
                }
                catch
                {
                    return false;
                }*/
            }
        }

        /// <summary>
        /// Start to check for any updates for the launcher itself.
        /// </summary>
        /// <param name="usedButton">If the user used the button in options menu.</param>
        public void CheckForUpdates(bool usedButton = false)
        {
            if (!IsServerOnline)
            {
                if(usedButton)
                {
                    MessageBox.Show("The server appears to be down, or there is no internet connection. Check back later.");
                }

                mainWindow.AddConsoleLine("(Updater) No updates available at this time.");
            }
            else
            {
                EpicVersionData newVersionData = EpicVersionData.Identify(FetchVersionData());
                if (newVersionData == null)
                    return;
                mainWindow.AddConsoleLine("(Updater) Current version: " + mainWindow.version);
                mainWindow.AddConsoleLine("(Updater) Latest version: " + newVersionData.version.ToString());

                if (mainWindow.version < newVersionData.version)
                {
                    mainWindow.AddConsoleLine("(Updater) An update is available!");

                    DialogResult dialogResult = MessageBox.Show("This launcher is outdated (" + mainWindow.version + "). Press OK to replace this version with version " + newVersionData.version + ".", "New Version Available", MessageBoxButtons.OKCancel);

                    if (dialogResult == DialogResult.OK)
                    {
                        new UpdateProgressWindow(mainWindow, newVersionData);
                    }
                }
                else if (usedButton)
                {
                    MessageBox.Show("There are no updates available at this time.");
                }
            }
        }

        public string FetchVersionData()
        {
            // used to build entire input
		    StringBuilder sb  = new StringBuilder();

		    // used on each read operation
		    byte[]        buf = new byte[8192];

            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(mainVersionURL);

            // execute the request
            HttpWebResponse response = (HttpWebResponse)
                request.GetResponse();
            // we will read data via the response stream
            Stream resStream = response.GetResponseStream();
            string tempString = null;
            int count = 0;

            do
            {
                // fill the buffer with data
                count = resStream.Read(buf, 0, buf.Length);

                // make sure we read some data
                if (count != 0)
                {
                    // translate from bytes to ASCII text
                    tempString = Encoding.ASCII.GetString(buf, 0, count);

                    // continue building the string
                    sb.Append(tempString);
                }
            }
            while (count > 0); // any more data to read?

            return sb.ToString();
        }
    }
}
