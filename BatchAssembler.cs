﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace Launcher
{
    public class BatchAssembler
    {
        private MainWindow mainWindow;
        private LaunchInfo launchInfo;
        private MCIdentifier mcIdentity, forgeIdentity;

        public BatchAssembler(MainWindow mainWindow, LaunchInfo launchInfo)
        {
            this.mainWindow = mainWindow;
            this.launchInfo = launchInfo;

            // We save the current credentials now for the next time.
            mainWindow.SaveCredentials(launchInfo.username, launchInfo.password);

            // Make sure that the used directories exist, just as a precaution.
            if (!DirectoriesInstalled)
            {
                mainWindow.AddConsoleLine(
                    "(BatchAssembler) Some directories that are needed could not be found."
                    + " Is minecraft installed and launched at least once ?");
            }

            // Make sure the natives are installed.
            if (!NativesExtracted)
                ExtractNatives();

            ReadJSONFiles();
            CheckDependencies();
            AssembleArguments();
            
        }

        /// <summary>
        /// Check if all directories that will be used,
        /// exist.
        /// </summary>
        private bool DirectoriesInstalled
        {
            get
            {
                List<string> directoriesToCheck = new List<string>();

                directoriesToCheck.Add(MainWindow.minecraftDir);
                directoriesToCheck.Add(MainWindow.nativesDir);


                foreach (string directory in directoriesToCheck)
                {
                    if (!Directory.Exists(directory))
                        return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Checks if there are any natives extracted.
        /// </summary>
        private bool NativesExtracted
        {
            get
            {
                if(Directory.Exists(MainWindow.nativesDir))
                    return false;
                foreach (string nativeFileName in MainWindow.nativesFileNames)
                {
                    if(!File.Exists(MainWindow.nativesDir + @"\" + nativeFileName))
                        return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Extract Minecraft's platform natives.
        /// The natives need to be extracted so this program can 
        /// read the .dll's that are packed inside the jar file.
        /// </summary>
        private void ExtractNatives()
        {
            // Check if the natives have already been extracted.
            if (NativesExtracted)
                return;

            mainWindow.AddConsoleLine("(BatchAssembler) Extracting natives.");

            string nativesFileName = launchInfo.launchSettings.launcherSettings.nativesVersion;

            // In this version, only 2.9.0 is supported.
            if (!File.Exists(MainWindow.nativesDir + @"\" + nativesFileName))
            {
                mainWindow.AddConsoleLine("(BatchAssembler) Error: Couldn't find the natives file " + nativesFileName);
                return;
            }

            // Extraction time.
            ArchiveHandler.ExtractJar(MainWindow.nativesDir + @"\" + nativesFileName, MainWindow.nativesDir);
        }

        /// <summary>
        /// Reads the JSON files in the version's folder.
        /// We'll be able to read about it's dependencies.
        /// </summary>
        private void ReadJSONFiles()
        {
            string mcVersion = launchInfo.launchSettings.launcherSettings.minecraftVersion.Replace(".jar", String.Empty);
            string forgeVersion = launchInfo.launchSettings.launcherSettings.forgeVersion.Replace(".jar", String.Empty);;
            string mcFileName = mcVersion + ".json";
            string forgeFileName = forgeVersion + ".json";
            string mcFilePath = MainWindow.minecraftVersionsDir + @"\" + mcVersion + @"\" + mcFileName;
            string forgeFilePath = MainWindow.minecraftVersionsDir + @"\" + forgeVersion + @"\" + forgeFileName;

            mcIdentity = MCIdentifier.parseFile(mcFilePath);
            forgeIdentity = MCIdentifier.parseFile(forgeFilePath);

            if(mcIdentity == null)
                mainWindow.AddConsoleLine("(BatchAssembler) Could not find the file at > " + mcFilePath);
            if (launchInfo.launchSettings.launchWithForge && forgeIdentity == null)
                mainWindow.AddConsoleLine("(BatchAssembler) Could not find the file at > " + forgeFilePath);
        }

        /// <summary>
        /// Checks all libraries depending on startup options.
        /// If there are jar files missing, it will be noted in the console.
        /// </summary>
        private void CheckDependencies()
        {
            if(mcIdentity == null || mcIdentity.libraries == null)
            {
                mainWindow.AddConsoleLine("(BatchAssembler) Couldn't parse libraries because it is either missing some libraries or doesnt exist at all.");

                return;
            }
            if (mcIdentity.libraries.Length == 0)
            {
                mainWindow.AddConsoleLine("(BatchAssembler) Unable to find or read the " +
                                            "minecraft JSON file.");
                return;
            }
            if (launchInfo.launchWithForge && forgeIdentity.libraries.Length == 0)
            {
                mainWindow.AddConsoleLine("(BatchAssembler) Unable to find or read the " +
                                            "forge JSON file.");
                return;
            }

            // Scan for missing libraries.
            foreach (string fileName in GetMissingLibraries(mcIdentity))
            {
                mainWindow.AddConsoleLine("(BatchAssembler) Couldn't find " + fileName);
            }

            if (launchInfo.launchWithForge)
            {
                foreach (string fileName in GetMissingLibraries(forgeIdentity))
                {
                    mainWindow.AddConsoleLine("(BatchAssembler) Couldn't find " + fileName);
                }
            }

            // Optional TODO: Notify user of missing libraries with MessageBox ?.
        }

        /// <summary>
        /// Read and find all libraries, and return the missing ones.
        /// </summary>
        /// <param name="identifier">The identifier containing the libraries.</param>
        /// <returns>A string ArrayList of missing filenames.</returns>
        private ArrayList GetMissingLibraries(MCIdentifier identifier)
        {
            string[] libsPresent = Directory.GetFiles(
                MainWindow.appData + @"\.minecraft\libraries",
                "*.jar", SearchOption.AllDirectories);
            ArrayList missingLibraries = new ArrayList();

            foreach (MCIdentifier.Lib lib in identifier.libraries)
            {
                // The first step is to get something
                // useful out of lib.name, which is currently:
                // org.lwjgl.lwjgl:lwjgl:2.9.0  (for example)
                
                // We split it into an array, in our case it's length will be 3.
                // [0] org.lwjgl.lwjgl
                // [1] lwjgl
                // [2] 2.9.0
                string[] libSplit = lib.name.Split(':');

                if (libSplit.Length != 3)
                {
                    mainWindow.AddConsoleLine("(BatchAssembler) The library " + lib.name +
                        " has an incorrect format. It should only have 3 of ':'.");
                    return null;
                }

                // Now we want to get the name of the actual jar file
                // with the information we have gatherered so far.
                string jar = libSplit[1] + "-" + libSplit[2] + ".jar";

                if (jar.Contains("platform"))
                    jar = jar.Substring(0, jar.Length - 4) + "-natives-windows" + ".jar";

                if (!(jar.Contains(launchInfo.launchSettings.launcherSettings.minecraftVersion) &&
                    jar.Contains(launchInfo.launchSettings.launcherSettings.forgeVersion) &&
                    jar.Contains("debug")))
                {
                    bool present = false;
                    foreach (string libPath in libsPresent)
                    {
                        if (libPath.Contains(jar))
                            present = true;
                    }
                    if (!present)
                        missingLibraries.Add(jar);
                            
                }
            }
            return missingLibraries;
        }

        /// <summary>
        /// Read and find all libraries.
        /// </summary>
        /// <param name="identifier">The identifier containing the libraries.</param>
        /// <returns>A string ArrayList of paths to each library file.</returns>
        private ArrayList GetLibraries(MCIdentifier identifier)
        {
            string[] libsPresent = Directory.GetFiles(
                MainWindow.appData + @"\.minecraft\libraries",
                "*.jar", SearchOption.AllDirectories);
            ArrayList libraries = new ArrayList();

            foreach (MCIdentifier.Lib lib in identifier.libraries)
            {
                // The first step is to get something
                // useful out of lib.name, which is currently:
                // org.lwjgl.lwjgl:lwjgl:2.9.0  (for example)

                // We split it into an array, in our case it's length will be 3.
                // [0] org.lwjgl.lwjgl
                // [1] lwjgl
                // [2] 2.9.0
                string[] libSplit = lib.name.Split(':');

                if (libSplit.Length != 3)
                {
                    mainWindow.AddConsoleLine("(BatchAssembler) The library " + lib.name +
                        " has an incorrect format. It should only have 3 of ':'.");
                    return null;
                }

                // Now we want to get the name of the actual jar file
                // with the information we have gatherered so far.
                string jar = libSplit[1] + "-" + libSplit[2] + ".jar";

                if (jar.Contains("platform"))
                    jar = jar.Substring(0, jar.Length - 4) + "-natives-windows" + ".jar";

                if (!(jar.Contains(launchInfo.launchSettings.launcherSettings.minecraftVersion) &&
                    jar.Contains(launchInfo.launchSettings.launcherSettings.forgeVersion) &&
                    jar.Contains("debug")))
                {
                    if (libsPresent.Contains(jar))
                        libraries.Add(jar);
                }
            }
            return libraries;
        }

        private void AssembleArguments()
        {
            /* Java and JavaW Syntax:
             * java [ options ] <class> [ arguments ... ]
             * java [ options ] -jar <file.jar> [ arguments ... ]
             * javaw [ options ] <class> [ arguments ... ]
             * javaw [ options ] -jar <file.jar> [ arguments ... ]
             */

            BatchBuilder batch = new BatchBuilder(mainWindow);

            // The initialized program.
            batch.AddArgument("javaw");

            // Specify a custom library path, the natives.
            // Without natives, minecraft simply won't launch.
            batch.AddArgument("-Djava.library.path=\"" + MainWindow.nativesDir + "\"");

            // Specify the classpath.
            // This is a pretty long one, so hang on to yer helmet.
            string mcFileName = launchInfo.launchSettings.launcherSettings.minecraftVersion;
            string mcJarPath = MainWindow.minecraftVersionsDir + @"\" + mcFileName.Replace(".jar", String.Empty) +
                @"\" + mcFileName;
            string mainClassName = String.Empty;

            string classPathArgument = String.Empty;
            string cpSeparator = ";";

            classPathArgument += "-cp " + "\"";
            classPathArgument += mcJarPath; // Include the minecraft jar so it can find the main class.
            classPathArgument += cpSeparator;

            MCIdentifier identifier;

            // Select the correct identifier so we can add all
            // the needed library paths to the argument.
            if (launchInfo.launchWithForge)
            {
                identifier = forgeIdentity;
                mainClassName = "net.minecraft.launchwrapper.Launch";
            }
            else
            {
                identifier = mcIdentity;
                mainClassName = "net.minecraft.client.main.Main";
            }

            // Cycle through all required libraries.
            for (int i = 0; identifier.libraries != null && i < identifier.libraries.Length; i++ )
            {
                MCIdentifier.Lib lib = identifier.libraries[i];
                // We split it into an array, in our case it's length will be 3.
                // [0] org.lwjgl.lwjgl
                // [1] lwjgl
                // [2] 2.9.0
                string[] libSplit = lib.name.Split(':');

                // Now we want to get the name of the actual jar file
                // with the information we have gatherered so far.
                string jarName = libSplit[1] + "-" + libSplit[2] + ".jar";

                FileInfo libFile = MainWindow.GetLibrary(jarName);
                if (libFile != null)
                {
                    mainWindow.AddConsoleLine("(BatchAssembler) Loading " + jarName);
                    classPathArgument += libFile.FullName;
                    classPathArgument += cpSeparator; // This semicolon is also magic. The last one must be outside of the path reference.
                }

            }

            classPathArgument += "\""; // NOTE: There is a space up front here which is very important.
            batch.AddArgument(classPathArgument);

            // Next, mention the main class so it knows where to find the entry point.
            batch.AddArgument(mainClassName);

            // Forge also requires an additional tweak class.
            if(launchInfo.launchWithForge)
            {
                batch.AddArgument("--tweakClass cpw.mods.fml.common.launcher.FMLTweaker");
            }

            // The username (email or legacy username)
            batch.AddArgument("--username=\"" + launchInfo.token.selectedProfile.name + "\"");

            // The session key.
            // It's called accessToken in 1.7.x
            batch.AddArgument("--session " + launchInfo.token.accessToken);

            // The game directory should point to the .minecraft folder.
            batch.AddArgument("--gameDir \"" + MainWindow.minecraftDir + "\"");

            // The assets directory. It contains all your worlds/savegames.
            batch.AddArgument("--assetsDir \"" + MainWindow.minecraftDir + @"\assets\");

            // Optionally, add our own JVM arguments that can be changed in the options.
            //batch.AddArgument(launchInfo.launchSettings.launcherSettings.jvmArguments);

            // 1.7.x things for later:
            //launchArgs += " --userProperties {}";
            //launchArgs += " --clientToken " + launchInfo.UID; //uuid

            mainWindow.AddConsoleLine("INFO: Launch arguments prepared successfully.");

            batch.Execute();
        }
    }
}
