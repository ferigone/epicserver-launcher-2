﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;

namespace Launcher
{
    public class EpicVersionData
    {
        public int version { get; set; }
        public string downloadURL { get; set; }
        public bool resetSettings { get; set; }

        /// <summary>
        /// Get a proper class back from reading the target JSON file.
        /// </summary>
        /// <param name="targetFile">Path to the file (any extension, but usually .json or .txt)</param>
        /// <returns>The GCLVersionData object that reflects the targeted json file.</returns>
        public static EpicVersionData Identify(string content)
        {
            EpicVersionData versionData = null;
            try
            {
                versionData = JsonConvert.DeserializeObject<EpicVersionData>(content);
                return versionData;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't parse Json File:");
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Create a MCIdentifier. It is unused, but might be useful in the future,
        /// when custom libraries need to be added automatically rather than manually.
        /// </summary>
        /// <param name="identifier">Creates a JSON file in Minecraft format.</param>
        /// <param name="TargetFile">for example @"C:\myIdentifier.json"</param>
        /// <returns>True if the operation succeeded.</returns>
        public static void Create(EpicVersionData versionData)
        {
            try
            {
                JsonSerializerSettings jsonSettings = new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };
                string json = JsonConvert.SerializeObject(versionData, Formatting.Indented);

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"/GCLVersion.json"))
                {
                    file.WriteLine(json);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't write Json File:");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
