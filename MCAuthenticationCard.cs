﻿using System;
using Newtonsoft.Json;

namespace Launcher
{
    public class MCAuthenticationCard
    {
        public struct Agent
        {
            public string name;
            public int version;
        }

        public Agent agent;
        public string username;
        public string password;
        public bool requestUser;

        public MCAuthenticationCard()
        {
            // Do nothing, make it possible to create a card without initializer variables.
        }

        public MCAuthenticationCard(string username, string password)
        {
            this.username = username;
            this.password = password;
            this.agent = new Agent();
            this.agent.name = "Minecraft";
            this.agent.version = 1;
            this.requestUser = true;
        }

        /// <summary>
        /// Parse a json string to the MCAuthenticationCard class
        /// </summary>
        /// <returns>the parsed MCAuthenticationCard object</returns>
        public static MCAuthenticationCard parse(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<MCAuthenticationCard>(jsonString);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't parse Json: ");
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Creates a MCAuthenticationCard json.
        /// </summary>
        /// <param name="card">The MCAuthenticationCard to create a json of</param>
        /// <returns>the generated json</returns>
        public string createJson()
        {
            try
            {
                JsonSerializerSettings jsonSettings = new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };
                return JsonConvert.SerializeObject(this, Formatting.Indented);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't generate Json: ");
                Console.WriteLine(ex.Message);
                return String.Empty;
            }
        }
    }

}
