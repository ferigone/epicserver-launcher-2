﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Launcher
{
    public class BatchBuilder
    {
        private MainWindow mainWindow;
        private List<string> arguments;

        /// <summary>
        /// Initialize the class.
        /// </summary>
        /// <param name="mainWindow">The caller/creator</param>
        public BatchBuilder(MainWindow mainWindow)
        {
            arguments = new List<string>();
            this.mainWindow = mainWindow;
        }

        /// <summary>
        /// Adds an argument to the list.
        /// </summary>
        /// <param name="argument">The argument</param>
        public void AddArgument(string argument)
        {
            arguments.Add(argument);
        }

        /// <summary>
        /// Build the current list of arguments and returns
        /// it as a completed string.
        /// </summary>
        /// <returns></returns>
        public string Build()
        {
            string arguments = String.Empty;
            string separator = " ";

            foreach(string argument in this.arguments)
            {
                arguments += argument;
                arguments += separator;
            }

            return arguments;
        }

        /// <summary>
        /// Executes the current batch argument in
        /// a new command line process.
        /// </summary>
        public void Execute()
        {
            try
            {
                Process process = new Process();

                // Build the Process's start information.
                ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe", "/c " + Build());
                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardError = true;
                startInfo.RedirectStandardOutput = true;
                startInfo.WindowStyle = ProcessWindowStyle.Maximized;

                mainWindow.AddConsoleLine(startInfo.Arguments);

                // Start the process with the built start information.
                process.StartInfo = startInfo;
                process.Start();

                while (process.StandardOutput.Peek() > -1)
                {
                    mainWindow.AddConsoleLine("(Java) " + process.StandardOutput.ReadLine());
                }

                while (process.StandardError.Peek() > -1)
                {
                    mainWindow.AddConsoleLine("(Java) Error: " + process.StandardError.ReadLine());
                }
            }
            catch (System.Exception ex)
            {
                mainWindow.AddConsoleLine("(BatchBuilder) Error: " + ex.Message);
            }
            
        }
    }
}
