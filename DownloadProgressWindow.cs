﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;

namespace Launcher
{
    public partial class DownloadProgressWindow : Form
    {
        private const bool SHOW_OVERWRITE_MSG = false;

        private const string titlePrefix = "Downloading ";
        private MainWindow mainWindow;
        private MCIdentifier identifier;

        private int currentLibraryIndex = 0;

        private MCIdentifier.Lib currentLib;
        private string package, name, version;
        private string fileName, targetPath, downloadUrl;

        public DownloadProgressWindow(MainWindow mainWindow, MCIdentifier identifier)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.identifier = identifier;
            currentLibraryIndex = 0;
            
            Show();
            StartDownload();
        }

        /// <summary>
        /// Start a chain which downloads each library depending on
        /// which index number / library number of identifier.libraries.
        /// </summary>
        private void StartDownload()
        {
            if (mainWindow == null)
            {
                // User closed the program.
                Close();
            }

            MCIdentifier.Lib currentLib = identifier.libraries[currentLibraryIndex];

            string[] idSplit = currentLib.name.Split(':');
            package = idSplit[0].Replace('.','/');
            name = idSplit[1];
            version = idSplit[2];

            CheckDirectories();

            fileName = String.Format("{0}-{1}.jar", name, version);
            targetPath = String.Format(@"{0}\libraries\{1}\{2}\{3}\{2}-{3}.jar", MainWindow.minecraftDir, package, name, version);
            downloadUrl = SplitToURL(idSplit);

            if(currentLib.natives.windows != null)
                targetPath.Insert(targetPath.Length - 4, "-natives-windows");

            CheckUrlFileExists(downloadUrl);
        }

        private void CheckUrlFileExists(string url)
        {
            HttpWebResponse response = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";
            request.Timeout = 5000;

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                /* A WebException will be thrown if the status of the response is not `200 OK` */
                mainWindow.AddConsoleLine(String.Format("(Installer) Error: {0}", ex.Message));
            }
            finally
            {
                // Don't forget to close your response.
                if (response != null)
                    response.Close();

                ContinueDownload();
            }
        }

        private void ContinueDownload()
        {
            string result = string.Empty;
            if(File.Exists(targetPath))
            {
                // Source: http://stackoverflow.com/questions/281640/how-do-i-get-a-human-readable-file-size-in-bytes-abbreviation-using-net
                // Credit goes to David Thibault.
                // --------------------------------------------
                string[] sizes = { "B", "KB", "MB", "GB" };
                double len = new FileInfo(targetPath).Length;
                int order = 0;
                while (len >= 1024 && order + 1 < sizes.Length)
                {
                    order++;
                    len = len / 1024;
                }
                // --------------------------------------------

                // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
                // show a single decimal place, and no space.
                result = String.Format("{0:0.##} {1}", len, sizes[order]);
            }
                
            string boxTitle = "Overwrite file ?";
            string boxMessage = String.Format("Do you want to overwrite the existing file {0} {1}", fileName, result);
            bool overWriteResult = (SHOW_OVERWRITE_MSG ? MessageBox.Show(boxMessage, boxTitle, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No : false);

            if (File.Exists(targetPath) && overWriteResult)
            {
                string skipMsg = String.Format("(Installer) Skipping {0} because it already exists.", fileName);
                mainWindow.AddConsoleLine(skipMsg);
                currentLibraryIndex++;
                if (currentLibraryIndex >= (identifier.libraries.Length - 1))
                    FinishedAllDownloads();
                else
                    StartDownload();

                return;
            }



            WebClient webClient = new WebClient();
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

            try
            {
                webClient.DownloadFileAsync(new Uri(downloadUrl), targetPath);
            }
            catch (WebException webex)
            {
                Console.WriteLine("Web Exception {0}", webex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception {0}", ex.Message);
            }
        }

        /// <summary>
        /// Check if the directories exist.
        /// If not, create them.
        /// </summary>
        private void CheckDirectories()
        {
            string currentDir = String.Format(@"{0}\", MainWindow.minecraftDir);
            if (!Directory.Exists(currentDir))
                Directory.CreateDirectory(currentDir);

            currentDir += @"libraries\";
            if (!Directory.Exists(currentDir))
                Directory.CreateDirectory(currentDir);

            currentDir += String.Format(@"{0}\", package);
            if (!Directory.Exists(currentDir))
                Directory.CreateDirectory(currentDir);

            currentDir += String.Format(@"{0}\", name);
            if (!Directory.Exists(currentDir))
                Directory.CreateDirectory(currentDir);

            currentDir += String.Format(@"{0}\", version);
            if (!Directory.Exists(currentDir))
                Directory.CreateDirectory(currentDir);
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (mainWindow == null)
            {
                // User closed the program.
                Close();
            }

            title.Text = String.Format("{0}% - Downloading {1}-{2}.jar", e.ProgressPercentage.ToString(), name, version);
            progressBar.Value = e.ProgressPercentage;
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            string finishedMsg = String.Format("Finished downloading {0}-{1}.jar", name, version);
            mainWindow.AddConsoleLine(finishedMsg);

            if (currentLibraryIndex >= (identifier.libraries.Length - 1))
                FinishedAllDownloads();
            else
            {
                currentLibraryIndex++;
                StartDownload();
            }

            
        }

        private void FinishedAllDownloads()
        {
            mainWindow.AddConsoleLine("(Install) Finished downloading all files.");
            //mainWindow.DoStartUpCheck();

            mainWindow.WindowState = FormWindowState.Normal;
            mainWindow.ShowInTaskbar = true;

            this.Close();
        }

                /// <summary>
        /// Turn a package, name, and version into an URL of the location
        /// of the given library.
        /// </summary>
        /// <param name="idSplit"></param>
        /// <returns></returns>
        private string SplitToURL(string[] idSplit)
        {
            if (idSplit.Length != 3)
            {
                mainWindow.AddConsoleLine("(Install) ID Split corrupted.");
                return null;
            }

            string package = idSplit[0].Replace('.', '/');
            string name = idSplit[1];
            string version = idSplit[2];

            return String.Format(@"https://libraries.minecraft.net/{0}/{1}/{2}/{1}-{2}.jar", package, name, version);
        }
    }
}
